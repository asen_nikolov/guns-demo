<?php

namespace app\components;
use yii\db\Query;

/**
 * Created by PhpStorm.
 * User: Asen
 * Date: 5.7.2016 г.
 * Time: 22:49
 */
class CActiveRecord extends \yii\db\ActiveRecord
{
    public static function findAllNoCondition($returnArray = false){
        if($returnArray === true){
            $q = new Query();
            $res = $q->select('*')->from(static::tableName())->all();
            return $res;
        }
        return static::find()->all();
    }
}