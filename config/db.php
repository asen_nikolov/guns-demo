<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=DBNAME',
    'username' => 'USER',
    'password' => 'PASS',
    'charset' => 'utf8',
];
