<?php

namespace app\controllers;

use app\models\CoreUser;
use app\models\Order;
use app\models\Product;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\HttpException;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout', 'show-cart', 'view-order', 'verify-hash', 'render-cart', 'remove-item'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->manageSession();
        $hash = Yii::$app->session->get('hash');
        $products = Product::findAllNoCondition();
        return $this->render('index', ['products' => $products, 'hash' => $hash]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAddProduct()
    {
        if (Yii::$app->request->isAjax) {
            $this->manageSession();
            $hash = Yii::$app->session->get('hash');
            $productId = intval(Yii::$app->request->post('productId'));
            $product = Product::findOne($productId);
            if (!$product) {
                throw new HttpException(400, 'No such product!');
            }
            $quantity = intval(Yii::$app->request->post('quantity'));
            $orderModel = Order::findOne(['hash' => $hash, 'status' => 0]);
            if (!$orderModel) {
//                $sql = "SELECT AUTO_INCREMENT FROM information_schema.`TABLES` WHERE TABLE_SCHEMA = 'test' AND TABLE_NAME = 'users'";
//                $nextUserId = Yii::$app->db->createCommand($sql)->execute();
                $user = CoreUser::createNewUser();
            } else {
                $currentDay = date('Y-m-d');
                if ($orderModel->valid_until < $currentDay) {
                    $this->clearInvalidData($orderModel);
                    $user = CoreUser::createNewUser();
                } else
                    $user = CoreUser::findOne($orderModel->user_id);
            }

            $order = Order::findOne([
                'hash' => $hash,
                'user_id' => $user->id,
                'product_id' => $productId,
                'status' => 0
            ]);
            if (empty($order)) {
                $order = new Order();
                $order->hash = $hash;
                $order->product_id = $productId;
                $order->user_id = $user->id;
                $order->status = 0;
            }

            $order->quantity += $quantity;
            $order->price = $product->price;
            $endDate = new \DateTime();
            $endDate->modify('+5 day');
            $order->valid_until = $endDate->format('Y-m-d');
            $order->save();

            // update all rows of the order with the new date end validity
            Yii::$app->db->createCommand()
                ->update(Order::tableName(), [
                    'valid_until' => $endDate->format('Y-m-d')
                ], 'hash=:hash', [':hash' => $order->hash])->execute();
            $q = new Query();
            $totalNumberOfProducts = $q->select('SUM(quantity)')
                ->from(Order::tableName())
                ->where('hash=:hash', [':hash' => $hash])->createCommand()->queryScalar();
            return json_encode([$quantity, $totalNumberOfProducts, $hash]);
        }
    }

    public function actionShowCart($hash = null)
    {
        $this->manageSession();
        $hash = Yii::$app->session->get('hash');
        $order = Order::findOne(['hash' => $hash]);
        $currentDay = date('Y-m-d');
        if ($order && $order->valid_until < $currentDay) {
            $this->clearInvalidData($order);
            $user = CoreUser::createNewUser();
        }
        if (!empty($_POST['ids']) && !empty($_POST['quantities']) && !empty($_POST['email'])) {
            $productIds = $_POST['ids'];
            $quantities = $_POST['quantities'];
            $username = strip_tags($_POST['name']);
            $email = strip_tags(filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $order = Order::findOne(['hash' => $hash]);
                $user = CoreUser::findOne($order->user_id);
                $user->name = $username;
                $user->email = $email;
                $user->save();
                foreach ($productIds as $key => $productId) {
                    Yii::$app->db->createCommand()
                        ->update(Order::tableName(), [
                            'quantity' => $quantities[$key],
                            'status' => 1
                        ], 'hash=:hash AND product_id=:id',
                            [
                                ':hash' => $hash,
                                ':id' => $productId,
                            ])->execute();
                }

                $transaction->commit();
                $orders = Order::findAll(['hash' => $hash, 'status' => 1]);
                $totalSum = 0;
                foreach ($orders as $order) {
                    $totalSum += $order->quantity * $order->price;
                }
                Yii::$app->session->setFlash('success', 'Your order has been received! Thank you!');
                Yii::$app->session->set('orderSuccess', 1);
                $message = $this->renderPartial('_emailMessage', ['user' => $user, 'hash' => $hash, 'orders' => $orders, 'totalSum' => $totalSum]);
                $headers = "Content-type: text/html; charset=UTF-8\r\n";
                mail($email, 'Your order from our Gun site', $message, $headers);
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Something went wrong!');
                Yii::$app->session->set('orderSuccess', 0);
            }
        }
        $orders = Order::findAll(['hash' => $hash, 'status' => 0]);

        // update the prices for the current order
        foreach ($orders as $order) {
            $order->price = $order->getProduct()->one()->price;
            $order->save();
        }
        return $this->render('showCart', ['orders' => $orders]);
    }

    public function actionViewOrder()
    {
        $this->manageSession();
        $hash = $_GET['hash'];
        $order = Order::findOne(['hash' => $hash]);
        if (empty($order)) {
            return $this->render('error');
        }

        $orders = Order::findAll(['hash' => $hash]);
        return $this->render('viewOrder', ['orders' => $orders]);
    }

    public function actionVerifyHash()
    {
        $this->manageSession();
        /* @var $order Order */
        $hash = $_POST['hash'];
        $order = Order::findOne(['hash' => $hash, 'status' => 0]);
        if ($order) {
            if ($order->valid_until < date('Y-m-d')) {
                // the order is expired
                $this->clearInvalidData($order);
                $hash = $this->generateHash();
            }
        } else {
            // no active order at all
            $hash = $this->generateHash();
        }
        Yii::$app->session->set('hash', $hash);
        return $hash;
    }

    public function actionRenderCart()
    {
        $this->manageSession();
        $hash = $_POST['hash'];
        $orders = Order::findAll(['hash' => $hash, 'status' => 0]);
        $totalSum = 0;
        foreach ($orders as $order) {
            $totalSum += $order->quantity * $order->price;
        }
        return $this->renderPartial('_renderCart', ['orders' => $orders, 'totalSum' => $totalSum]);
    }

    public function actionRemoveItem()
    {
        if (Yii::$app->request->isAjax) {
            $productId = $_POST['productId'];
            $hash = $_POST['hash'];
            Yii::$app->db->createCommand()
                ->delete(Order::tableName(), 'hash=:hash AND product_id=:id', [
                    ':hash' => $hash,
                    ':id' => $productId
                ])->execute();
        }
    }

    private function generateHash()
    {
        $date = microtime();
        $salt = mt_rand(0, 500000000);
        $hash = md5($date) . md5($salt);
        return sha1($hash);
    }

    private function manageSession()
    {
//        if ($hash) {
//            Yii::$app->session->set('hash', $hash);
//        } else {
//            if (!Yii::$app->session->get('hash'))
//                Yii::$app->session->set('hash', $this->generateHash());
//        }
        if (Yii::$app->session->get('orderSuccess') == 1)
            Yii::$app->session->destroy();
    }

    private function clearInvalidData(Order $orderModel)
    {
        Yii::$app->db->createCommand()
            ->delete(CoreUser::tableName(), 'id=:id', [':id' => $orderModel->user_id])->execute();
        Yii::$app->db->createCommand()
            ->delete(Order::tableName(), 'hash=:hash', ['hash' => $orderModel->hash])->execute();
    }
}
