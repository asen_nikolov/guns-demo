<?php

namespace app\models;

use app\components\CActiveRecord;
use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property string $id
 * @property string $hash
 * @property string $user_id
 * @property string $product_id
 * @property string $price
 * @property string $quantity
 * @property string $valid_until
 * @property string $status
 *
 * @property Users $user
 * @property Products $product
 */
class Order extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hash', 'user_id', 'product_id', 'price', 'quantity', 'valid_until'], 'required'],
            [['user_id', 'product_id', 'quantity', 'status'], 'integer'],
            [['price'], 'number'],
            [['hash', 'valid_until'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hash' => 'Hash',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'price' => 'Price',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(CoreUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
