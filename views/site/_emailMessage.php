<?php
/**
 * Created by PhpStorm.
 * User: Asen
 * Date: 8.7.2016 г.
 * Time: 23:19
 */
/* @var $user \app\models\CoreUser */
?>
Hello <?= $user->name ?> . ', you have successfully confirmed your order! You can view the order from
<a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['site/view-order', 'hash' => $hash]) ?>">HERE</a><br/>
Also here is your order again <br/>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
        <th style="border: 1px solid black;padding: 5px 8px">Product</th>
        <th style="border: 1px solid black;padding: 5px 8px">Quantity</th>
        <th style="border: 1px solid black;padding: 5px 8px">Price</th>
    </tr>
    <?php
    foreach ($orders as $order) { ?>
        <tr>
            <td style="border: 1px solid black;padding: 5px 8px"><?= $order->getProduct()->one()->name ?></td>
            <td style="border: 1px solid black;padding: 5px 8px"><?= $order->quantity ?></td>
            <td style="border: 1px solid black;padding: 5px 8px"><?= $order->price ?></td>
        </tr>
    <?php } ?>
</table>
<h2>TOTAL SUM: <?= number_format($totalSum, 2) ?></h2>

<br/>Thank you!
