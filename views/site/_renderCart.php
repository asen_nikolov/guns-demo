<?php
/**
 * Created by PhpStorm.
 * User: Asen
 * Date: 8.7.2016 г.
 * Time: 21:08
 */
use yii\bootstrap\ActiveForm;

if (!empty($orders)) {
    $form = ActiveForm::begin([
        'action' => Yii::$app->urlManager->createAbsoluteUrl('site/show-cart')
    ]); ?>
    <table class="table table-bordered">
        <tr>
            <th>Name of the item</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Total Price</th>
            <th></th>
        </tr>
        <?php
        /* @var $order \app\models\Order */
        foreach ($orders as $order) {
            $product = $order->getProduct()->one(); ?>
            <input type="hidden" name="ids[]" value="<?= $product->id ?>"/>
            <tr>
                <td><?= $product->name ?></td>
                <td>
                    <input class="quantity" type="number" min="1" value="<?= $order->quantity ?>" name="quantities[]"/>
                </td>
                <td class="price"><?= $product->price ?></td>
                <td class="totalPrice"><?= number_format($product->price * $order->quantity, 2) ?></td>
                <td>
                    <a href="#" data-id="<?=$product->id?>" class="deleteRow" title="Remove item!"><i class="fa fa-close fa-2x red"></i></a>
                </td>
            </tr>
        <?php } ?>
    </table>
    <div id="globalSum" class="pull-right" style="clear: both"><h1>Total: <?= number_format($totalSum, 2) ?></h1>
    </div>
    <div>
        <label for="name">Please, enter your name * </label>
        <input type="text" id="name" name="name" required/>
    </div>
    <div>
        <label for="email">Please, enter your email * </label>
        <input type="email" id="email" name="email" required/>
    </div>
    <?= \yii\helpers\Html::submitButton('Order now!', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end();
} else { ?>
    <h3 class="text-center">You haven't ordered yet anything!</h3>
<?php } ?>
