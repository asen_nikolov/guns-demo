<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Guns & Rouses';
?>
<div class="site-index">


    <div class="body-content">
        <div class="row">
            <?php
            /* @var $product \app\models\Product */
            foreach ($products as $product) { ?>
                <div class="item row-fluid">
                    <div class="col-sm-12"><?= $product->name ?>
                        <span class="priceContent">$ <?= $product->price ?></span>
                    </div>
                    <div class="col-sm-4">
                        <?= Html::textInput('price_' . $product->id, 1, ['min' => 1, 'type' => 'number']) ?>
                    </div>
                    <div class="col-sm-8">
                        <?= Html::button('Добави в кошницата', ['class' => 'btn btn-primary addOrder', 'data-id' => $product->id]) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(document).on('click', '.addOrder', function () {
            var productId = $(this).data('id');
            var quantity = $('input[name="price_' + productId + '"]').val();
            var url = '<?=Yii::$app->urlManager->createUrl('site/add-product')?>';
            $.ajax({
                type: 'POST',
                url: url,
                data: {productId: productId, quantity: quantity}
            }).success(function (data) {
                data = JSON.parse(data);
                $('#cartLink').text('Shopping cart: ' + data[1]);
                hash = data[2];
                localStorage.setItem('gun_order', hash);
                console.log(hash);
                alert('You have successfully added ' + data[0] + ' products!');
            }).error(function (request, status, error) {
                alert(request.responseText);
            })
        })
    })
</script>