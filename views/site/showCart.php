<?php
/**
 * Created by PhpStorm.
 * User: Asen
 * Date: 6.7.2016 г.
 * Time: 20:02
 */
use yii\bootstrap\ActiveForm;

?>

<div class="row">
    <div class="col-sm-12">
        <?php \app\components\Components::printFlashMessages(); ?>
        <div id="shoppingTable"></div>
    </div>
</div>
<script>
    function renderCart() {
        var localHash = localStorage.getItem('gun_order');
        var url = '<?=Yii::$app->urlManager->createAbsoluteUrl('site/render-cart')?>';
        $.ajax({
            url: url,
            type: 'POST',
            data: {hash: localHash}
        }).success(function (data) {
            $('#shoppingTable').html(data);
        });
    }

    $(function () {
        setTimeout(function () {
            renderCart();
        }, 500);

        $(document).on('input', '.quantity', function () {
            var newQuantity = $(this).val();
            var price = parseFloat($(this).parent().parent().find('td.price').text());
            var changeSum = (newQuantity * price).toFixed(2);
            $(this).parent().parent().find('td.totalPrice').text(changeSum);
            var sumsPerRow = $('td.totalPrice');
            var totalSum = 0;
            for (var i = 0; i < sumsPerRow.length; i++) {
                totalSum += parseFloat(sumsPerRow[i].innerText);
            }
            $('#globalSum h1').text('Total: ' + totalSum);
        });

        $(document).on('click', 'a.deleteRow', function () {
            var productId = $(this).data('id');
            $.ajax({
                url: '<?=Yii::$app->urlManager->createUrl('site/remove-item')?>',
                type: 'POST',
                data: {
                    productId: productId,
                    hash: localStorage.getItem('gun_order')
                }
            }).done(renderCart);
        });
    })
</script>