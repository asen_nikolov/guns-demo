<?php
/**
 * Created by PhpStorm.
 * User: Asen
 * Date: 7.7.2016 г.
 * Time: 23:03
 */
?>
<table id="shoppingTable" class="table table-bordered">
    <tr>
        <th>Name of the item</th>
        <th>Quantity</th>
        <th>Price</th>
    </tr>
    <?php
    /* @var $order \app\models\Order */
    foreach ($orders as $order) {
        $product = $order->getProduct()->one(); ?>
        <input type="hidden" name="ids[]" value="<?= $product->id ?>"/>
        <tr>
            <td><?= $product->name ?></td>
            <td>
                <?= $order->quantity ?>
            </td>
            <td><?= $product->price ?></td>
        </tr>
    <?php } ?>
</table>
